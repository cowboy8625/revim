![lastupdated](https://img.shields.io/github/last-commit/cowboy8625/ReVim)
![GitHub repo size](https://img.shields.io/github/repo-size/cowboy8625/ReVim)
![issuse](https://img.shields.io/github/issues/cowboy8625/ReVim)
![Discord](https://img.shields.io/discord/509849754155614230)
# ReVim

ReVim stands for "Rust Edition Vim".  I wanted a Simple vim that would compile and run on any terminal
like in windows. We all know Vim can be ran on windows but its not the easiest to do.
ALSO I just really wanted to make a text editor, so this was a cool project to do.
Currently ReVim is in a very early statge of progress.  If you would like to contribute
to ReVim submite a pull request.



# Dependency's
    * crossterm = "0.14.2"
    * ropey = "1.1.0"
    * clap = "2.33.1"

If you want to [compair][https://github.com/cowboy8625/ReVim/wiki/Comparisons] ReVim current state
with other popular editors.
